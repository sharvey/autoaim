﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class SimpleDispatchTest : MonoBehaviour
{
	public ComputeShader _assistShader;
	ComputeBuffer _buf;

	private void Awake()
	{
		_buf = new ComputeBuffer(1, 2 * sizeof(float));
	}

	private void OnPreCull()
	{
		var pts = new Vector2[1];
		pts[0] = Input.mousePosition;
		_buf.SetData(pts);

		Debug.Log("Raw: " + pts[0]);

		var cam = GetComponent<Camera>();
		var buf = new CommandBuffer();

		var kernel = _assistShader.FindKernel("AimAssist");
		//buf.BeginSample("Aim Assist");
		uint threadX, threadY, threadZ;
		_assistShader.GetKernelThreadGroupSizes(kernel, out threadX, out threadY, out threadZ);
		//buf.SetComputeTextureParam(_assistShader, kernel, "sdf", _rt);
		buf.SetComputeBufferParam(_assistShader, kernel, "points", _buf);
		buf.DispatchCompute(_assistShader, kernel, (int)threadX, (int)threadY, (int)threadZ);

		cam.RemoveAllCommandBuffers();
		cam.AddCommandBuffer(CameraEvent.AfterEverything, buf);
	}

	private void Update()
	{
		StartCoroutine(TestEOF());
	}

	IEnumerator TestEOF()
	{
		yield return new WaitForEndOfFrame();
		var newPoints = new Vector2[1];
		_buf.GetData(newPoints);
		Debug.Log("EOF:"+newPoints[0]);
	}

	private void OnPostRender()
	{
		var newPoints = new Vector2[1];
		_buf.GetData(newPoints);
		Debug.Log("POST:"+newPoints[0]);
	}
}
