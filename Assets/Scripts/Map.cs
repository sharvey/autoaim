﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

class CameraEvents : MonoBehaviour
{
	public event Action PreCullEvent = () => { };
	public event Action PreRenderEvent = () => { };
	public event Action PostRenderEvent = () => { };

	private void OnPreCull()
	{
		PreCullEvent();
	}

	private void OnPreRender()
	{
		PreRenderEvent();
	}

	private void OnPostRender()
	{
		PostRenderEvent();
	}
}

public class Map : MonoBehaviour
{
	Camera _cam;

	[SerializeField]
	private LayerMask _mask;

	[SerializeField]
	private RenderTexture _output = null;

	[SerializeField]
	private int _sdfSize = 32;

	private void Awake()
	{
		var camGo = new GameObject("Camera");
		camGo.transform.parent = transform;
		_cam = camGo.AddComponent<Camera>();
		camGo.hideFlags = HideFlags.DontSave;
		camGo.transform.localPosition = new Vector3(0, 0, -1);
		_cam.backgroundColor = Color.clear;
		_cam.nearClipPlane = 0.00001f;
		_cam.farClipPlane = 2.0f;
		_cam.orthographic = true;
		_cam.cullingMask = _mask;
		_cam.orthographicSize = 10.0f;
		_cam.targetTexture = _output;
		var events = camGo.AddComponent<CameraEvents>();
		events.PreCullEvent += Events_PreCullEvent;
		events.PostRenderEvent += Map_PostRenderEvent;
	}

	private void Events_PreCullEvent()
	{
		
	}

	private void Map_PostRenderEvent()
	{
		var sdf = SDF.Bake(_output, _sdfSize);
		Graphics.Blit(sdf, _output);
		RenderTexture.ReleaseTemporary(sdf);
	}

	private void Update()
	{
		//if (_cam.targetTexture)
		//{
		//	_cam.targetTexture.Release();
		//}
		//
		//_cam.targetTexture = RenderTexture.GetTemporary(Mathf.CeilToInt(transform.lossyScale.x * _pixelPerUnit), Mathf.CeilToInt(transform.lossyScale.y * _pixelPerUnit));
	}

	private void OnDrawGizmos()
	{
		Gizmos.matrix = transform.localToWorldMatrix;
		Gizmos.DrawWireCube(Vector3.zero, Vector3.one);
	}
}
