﻿using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using UnityEngine.Rendering;
using System;

/**
 * 
 **/

class Foo : MonoBehaviour
{
	/*public event Action PostRenderEvent;

	private void OnPostRender()
	{
		Debug.Log("Foo PostRender");
		PostRenderEvent();
	}*/
}

[RequireComponent(typeof(Camera))]
public class AimAssist : MonoBehaviour 
{
	[SerializeField]
	private LayerMask _layerMask = 0;

	[SerializeField]
	[Range(1, 8)]
	private int _downscale = 1;

	[SerializeField]
	[Range(0, 0xFF)]
	private int _sdfSize = 32;

	[SerializeField]
	[Range(0, 0xFF)]
	private int _numErosionPasses = 0;

	private Camera _camera;
	private Camera _shootableCam, _restCam;

	const int MaxPoints = 0xFFFF;

	[SerializeField]
	private RenderTexture _rt;

	//[Range(1, 2048)]
	//[SerializeField]
	//private int _sdfSize = 32;

	[SerializeField]
	private ComputeShader _assistShader = null;

	public ComputeBuffer _pointsBuffer;
//	private Vector2[] _rawPoints;
	private Vector2[] _adjustedPoints;

	public ReadOnlyCollection<Vector2> AdjustedPoints
	{
		get
		{
			//Debug.Log("Reading Adjusted points");
			return Array.AsReadOnly(_adjustedPoints);
		}
	}

	//private void Buffer { if (_buf!=null)_buf.Dispose} }

	public Vector2[] RawPoints;
	private MonoBehaviour _eofHandle;

	private void Start()
	{
		Camera.onPreCull += OnCameraPreCull;
		Camera.onPostRender += OnCameraPostRender;

		_pointsBuffer = new ComputeBuffer(MaxPoints, 2 * sizeof(float));
		_camera = GetComponent<Camera>();

		_restCam = CreateCamera("Others");
		_restCam.backgroundColor = Color.clear;
		_restCam.clearFlags = CameraClearFlags.SolidColor;
		_restCam.depth = _camera.depth;
		_restCam.cullingMask = ~_layerMask;

		_shootableCam = CreateCamera("Shootable");
		_shootableCam.depth = _camera.depth + 1;
		_shootableCam.clearFlags = CameraClearFlags.Nothing;
		_shootableCam.cullingMask = _layerMask;
		//_eofHandle = _shootableCam.gameObject.AddComponent<Foo>();
		//_shootableCam.gameObject.AddComponent<Foo>().PostRenderEvent += OnComputeBufferReady;

		_shootableCam.SetReplacementShader(Shader.Find("Unlit/AimAssist"), null);
		_restCam.SetReplacementShader(Shader.Find("Unlit/JustDepth"), null);
	}

	private void OnDestroy()
	{
		Camera.onPostRender -= OnCameraPostRender;
		if (_pointsBuffer != null)
		{
			_pointsBuffer.Release();
		}
	}

	private void OnCameraPreCull(Camera cam)
	{
		//Debug.Log(cam.name + " precull");
		if (cam == _shootableCam)
		{
			_OnPreCull(_shootableCam);
		}
	}

	private void OnCameraPostRender(Camera cam)
	{
		if (cam == _restCam)
		{
			_adjustedPoints = new Vector2[RawPoints.Length];
			_pointsBuffer.GetData(_adjustedPoints);
			//Debug.Log(cam.name + ":" + _adjustedPoints[0]);
		}
	}

	//private void Update()
	//{
	//	//StartCoroutine(FetchPointsAtEndOfFrame());
	//}

	/*IEnumerator FetchPointsAtEndOfFrame()
	{
		yield return new WaitForEndOfFrame();
		var _adjustedPoints = new Vector2[_pointsBuffer.count];
		_pointsBuffer.GetData(_adjustedPoints);
		Debug.Log("EOF:"+ _adjustedPoints[0]);
	}*/

	private void _OnPreCull(Camera cam)
	{
		//Debug.Log("Upload");
		_pointsBuffer.SetData(RawPoints);

		var w = _camera.pixelWidth / _downscale;
		var h = _camera.pixelHeight / _downscale;

		if (_rt)
		{
			RenderTexture.ReleaseTemporary(_rt);
		}

		_rt = RenderTexture.GetTemporary(w, h, 24);

		_rt.filterMode = FilterMode.Point;
		_shootableCam.targetTexture = _restCam.targetTexture = _rt;

		cam.RemoveAllCommandBuffers();
		if (_numErosionPasses > 0)
			_shootableCam.AddCommandBuffer(CameraEvent.AfterEverything, Erode(_numErosionPasses, _rt, _rt));
		_shootableCam.AddCommandBuffer(CameraEvent.AfterEverything, SDF.BakeCommandBuffer(_rt, _rt, _sdfSize));

		// if this command buffer is added to _camera, it works ?!
		cam.AddCommandBuffer(CameraEvent.AfterEverything, AimAssistCommandBuffer());
	}

	private Material _erosionMat;

	CommandBuffer Erode(int numPass, Texture input, RenderTexture output)
	{
		if (_erosionMat == null)
		{
			_erosionMat = new Material(Shader.Find("Hidden/Erode"));
		}

		var buf = new CommandBuffer()
		{
			name = "Erode"
		};

		int src = 1, dst = 2;

		buf.GetTemporaryRT(src, output.descriptor);
		buf.GetTemporaryRT(dst, output.descriptor);
		buf.Blit(input, src);

		for (int i=0; i<numPass; ++i)
		{
			buf.Blit(src, dst, _erosionMat);
			Swap(ref src, ref dst);
		}

		buf.Blit(src, output);
		buf.ReleaseTemporaryRT(src);
		buf.ReleaseTemporaryRT(dst);

		return buf;
	}

	CommandBuffer AimAssistCommandBuffer()
	{
		var buf = new CommandBuffer()
		{
			name = "Aim Assist"
		};

		//Debug.Log("Create assist command buffer");

		var kernel = _assistShader.FindKernel("AimAssist");
		uint threadX, threadY, threadZ;
		_assistShader.GetKernelThreadGroupSizes(kernel, out threadX, out threadY, out threadZ);

		buf.SetComputeTextureParam(_assistShader, kernel, "sdf", _rt);
		buf.SetComputeBufferParam(_assistShader, kernel, "points", _pointsBuffer);
		buf.DispatchCompute(_assistShader, kernel, RawPoints.Length, (int)threadY, (int)threadZ);

		return buf;
	}

	private Camera CreateCamera(string name)
	{
		var go = new GameObject(name);
		go.transform.parent = transform;
		go.transform.localPosition = Vector3.zero;
		var cam = go.AddComponent<Camera>();
		cam.CopyFrom(_camera);
		return cam;
	}

	private void OnRenderImage(RenderTexture source, RenderTexture destination)
	{
		Graphics.Blit(source, destination);
		Graphics.Blit(_rt, destination);
	}

	//http://kylehalladay.com/blog/tutorial/2014/06/27/Compute-Shaders-Are-Nifty.html
	public void AdjustAim(Vector2[] points)
	{
		/*ComputeBuffer buf = new ComputeBuffer(points.Length, 2 * sizeof(float));
		buf.SetData(points);
		var kernel = _assistShader.FindKernel("AimAssist");
		_assistShader.SetTexture(kernel, "sdf", _rt);
		_assistShader.SetFloat("sdfSize", _sdfSize);
		_assistShader.SetBuffer(kernel, "points", buf);
		_assistShader.Dispatch(kernel, points.Length, 1, 1);
		buf.GetData(points);
		buf.Dispose();*/
	}

	public static void Swap<T>(ref T a, ref T b)
	{
		T tmp = b;
		b = a;
		a = tmp;
	}
}
