﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestAimAssist : MonoBehaviour 
{
	[SerializeField]
	[Range(0, 1)]
	private float _spreadSize = 0.05f;

	const int NumPoints = 128;

	Vector2[] _points = new Vector2[NumPoints];
	Vector2[] _off = new Vector2[NumPoints];

	AimAssist _assist;

	[SerializeField]
	private Material _debugMat;

	private void Awake()
	{
		_assist = GetComponent<AimAssist>();
		StartCoroutine(TestEOF());

		for (int i=0; i<NumPoints; ++i)
		{
			_off[i] = Random.insideUnitCircle;
		}
	}

	private void Update()
	{
		Random.InitState(8008135);

		//if (_assist._pointsBuffer != null)
		//{
		//	_assist._pointsBuffer.GetData(_points);
		//	Debug.Log("Last frame buf:"+_points[0]);
		//}

		Vector2 center = new Vector2(.5f, .5f);// GetComponent<Camera>().ScreenToViewportPoint(Input.mousePosition);
		Vector2 spread = new Vector2(_spreadSize, _spreadSize * Screen.width / Screen.height);
		for (int i = 0; i < _points.Length; ++i)
		{
			_points[i] = center + new Vector2(_off[i].x * spread.x, _off[i].y * spread.y);
		}

		_assist.RawPoints = _points;

		

		//GetComponent<AimAssist>().AdjustAim(points);
	}

	//private void OnRenderImage(RenderTexture source, RenderTexture destination)
	//IEnumerator TestEOF()
	private IEnumerator TestEOF()
	{
		var mesh = new Mesh();
		var vertices = new Vector3[NumPoints * 2];
		var indices = new List<int>();
		
		for (int i=0; i<NumPoints; ++i)
		{
			indices.Add(i * 2);
			indices.Add(i * 2 + 1);
		}

		while (Application.isPlaying)
		{
			yield return new WaitForEndOfFrame();

			var points = _assist.AdjustedPoints;
			var original = _points;

			if (points == null || points.Count != original.Length)
			{
				Debug.LogWarning("still fucked up.");
				continue;
			}

			_debugMat.SetPass(0);
			//mesh.Clear();
			for (int i = 0; i < NumPoints; ++i)
			{
				vertices[i * 2] = points[i];
				vertices[i * 2 + 1] = original[i];
			}

			mesh.vertices = vertices;
			mesh.SetIndices(indices.ToArray(), MeshTopology.Lines, 0);
			GL.PushMatrix();
			GL.LoadOrtho();
			//Graphics.DrawMesh(mesh, Matrix4x4.identity, _debugMat, 0);
			Graphics.DrawMeshNow(mesh, Camera.current.transform.position, Camera.current.transform.rotation);
			GL.PopMatrix();
			//GL.LoadIdentity();
			/*GL.PushMatrix();
			GL.Begin(GL.LINES);
			GL.LoadOrtho();
			for (int i = 0; i < original.Length; ++i)
			{
				//Debug.Log(original[i] + "->" + points[i]);

				GL.Color(Color.white);
				GL.Vertex(original[i]);
				GL.Color(Color.green);
				GL.Vertex(points[i]);
			}
			GL.End();
			GL.PopMatrix();*/
		}

		//while (Application.isPlaying)
		//{
		//	yield return new WaitForEndOfFrame();
		//Graphics.Blit(source, destination);

		/*var original = _points;
		var points = _assist.AdjustedPoints;

		//GL.LoadIdentity();
		GL.PushMatrix();
		GL.LoadOrtho();
		GL.Begin(GL.LINES);
		for (int i=0; i< original.Length; ++i)
		{
			//Debug.Log(original[i] + "->" + points[i]);

			GL.Color(Color.white);
			GL.Vertex(original[i]);
			GL.Color(Color.green);
			GL.Vertex(points[i]);
		}
		GL.End();
		GL.PopMatrix();*/
		//}
	}

	//private void LateUpdate()
	//{
	//
	//}
}
