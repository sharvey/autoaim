﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class TestSDF_CommandBuffer : MonoBehaviour
{
	[SerializeField]
	private Texture _tex;

	[SerializeField]
	private RenderTexture _rt;

	[SerializeField]
	[Range(0, 2048)]
	private int _size = 64;

	[SerializeField]
	private float _spreadSize = .2f;

	[SerializeField]
	private ComputeShader _aimAssistCS;

	private Vector2[] _aimPositions;
	private ComputeBuffer _positionBuffer;

	private Camera _cam;

	private void Awake()
	{
		//_rt = new RenderTexture(512, 512, 0);
		_cam = GetComponent<Camera>();
		_positionBuffer = new ComputeBuffer(40, 2 * sizeof(float));
		//_positionBuffer = new ComputeBuffer()
		//_cam.AddCommandBuffer(CameraEvent.AfterEverything, SDF.BakeCommandBuffer(_tex, _rt, _size));
		//_cam.AddCommandBuffer(CameraEvent.AfterEverything, AimAssistCommandBuffer());
	}

	private Vector2 _prevSize = Vector2.zero;

	private void OnPreCull()
	{
		/*if (_prevSize != _cam.pixelRect.size)
		{
			_prevSize = _cam.pixelRect.size;
			Debug.Log("Size changed.");
			_cam.RemoveAllCommandBuffers();
		}*/

		//Debug.Log("PreCull");

		if (_rt) RenderTexture.ReleaseTemporary(_rt);
		_rt = RenderTexture.GetTemporary(_cam.pixelWidth, _cam.pixelHeight, 24);
		_cam.RemoveAllCommandBuffers();
		_cam.AddCommandBuffer(CameraEvent.AfterEverything, SDF.BakeCommandBuffer(_tex, _rt, _size));
		_cam.AddCommandBuffer(CameraEvent.AfterEverything, AimAssistCommandBuffer());

		
	}

	private void UpdatePoints()
	{
		Random.InitState(8008135);
		Vector2 center = GetComponent<Camera>().ScreenToViewportPoint(Input.mousePosition);
		const int N = 40;
		var points = new Vector2[N];
		var original = new Vector2[N];
		for (int i = 0; i < points.Length; ++i)
		{
			original[i] = points[i] = center + Random.insideUnitCircle * _spreadSize;
		}

		_aimPositions = original;
	}

	private void Update()
	{
		//Debug.Log("Update");
		UpdatePoints();
		StartCoroutine(FetchAtEndOfFrame());
	}

	private IEnumerator FetchAtEndOfFrame()
	{
		yield return new WaitForEndOfFrame();
		//Debug.Log("FetchPoints");
		_positionBuffer.GetData(_aimPositions);
	}

	CommandBuffer ErodeCommandBuffer()
	{
		var buf = new CommandBuffer();
		return buf;
	}

	CommandBuffer AimAssistCommandBuffer()
	{
		var buf = new CommandBuffer()
		{
			name = "Aim Assist"
		};

		buf.SetGlobalTexture("sdf", _rt);
		buf.SetGlobalBuffer("points", _positionBuffer);
		buf.DispatchCompute(_aimAssistCS, _aimAssistCS.FindKernel("AimAssist"), 1, 1, 1);
		return buf;
	}

	private void OnRenderImage(RenderTexture source, RenderTexture destination)
	{
		Graphics.Blit(_rt, destination);
	}
}
